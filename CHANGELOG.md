# [1.1.0](https://gitlab.com/glci/docker-semantic-release-gitlab-ci/compare/v1.0.2...v1.1.0) (2023-10-11)


### Features

* bumps the version ([7113ac2](https://gitlab.com/glci/docker-semantic-release-gitlab-ci/commit/7113ac240e8d367d3469242230d31fd943383e8a))

## [1.0.2](https://gitlab.com/glci/docker-semantic-release-gitlab-ci/compare/v1.0.1...v1.0.2) (2023-10-11)


### Bug Fixes

* corrects the Dockerfile ([638cb20](https://gitlab.com/glci/docker-semantic-release-gitlab-ci/commit/638cb2061f75e1f6d8e822866ef383cccce3add5))
* fixed the issue with the `/app` folder of the official NodeJS image ([986897c](https://gitlab.com/glci/docker-semantic-release-gitlab-ci/commit/986897c79dba4a0fea11b57cfd94f5c0d06b58b6))

## [1.0.1](https://gitlab.com/glci/docker-semantic-release-gitlab-ci/compare/v1.0.0...v1.0.1) (2021-08-19)


### Bug Fixes

* changes the Docker image and release rc file ([360c567](https://gitlab.com/glci/docker-semantic-release-gitlab-ci/commit/360c567e49e7c36cfebd84f8eee7c9df0bd15bc1))
