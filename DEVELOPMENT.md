# Development

To build and deploy the image to the container registry on GitLab after intervention on the image: 
```shell
docker login registry.gitlab.com

docker build -t registry.gitlab.com/glci/docker-semantic-release-gitlab-ci:[date_tag] .
docker push registry.gitlab.com/glci/docker-semantic-release-gitlab-ci:[date_tag]

docker tag registry.gitlab.com/glci/docker-semantic-release-gitlab-ci registry.gitlab.com/glci/docker-semantic-release-gitlab-ci:latest
```
